# A. Labyrinth modul

Labyrinth je modul do živé verze hry „Keep Talking and Nobody Explodes“. Modul muže fungovat jak s master modulem tak i zvlášť.

Existuje dvě možností použiti:
- Bez master modulu
- S zapojením master modulu k TX a RX pinům

V prvním případě hra začíná od spouštění modulu a pří dosažení cíli začíná další hra.

V druhém případě modul čeká na START zprávu od master modulu a pří doručení teto zprávy aktivuje ovládání modulem. Pří dosažení hráčem cíli hra je skončená, modul deaktivuje ovládání a posila WIN zprávu master modulu.

## Návod na použiti

Po startu hry, pomocí tlačítek je potřeba dovést žlutý čtverec k cíle (červny čtverec).
Více informaci je v anglickém manuálu hry „Keep Talking and Nobody Explodes“, který je uveden níž v tomto navodu.

```Cheat mode: spojit pin 7 a GND, a restartovat hru.```


## Hardware

 - Arduino Uno(Leonardo)
 - 4 tlačítka
 - Display ST7735

## Source
https://gitlab.fit.cvut.cz/maherula/maze

# Schema připojení

![Scheme](https://gitlab.fit.cvut.cz/maherula/maze/raw/master/Scheme.png)

# Manual
 - Find the maze with matching circular markings.
 - The defuser must navigate the yellow light to the red rectangle using the arrow buttons.
 - Warning: Do not cross the lines shown in the maze. These lines are invisible on the bomb.

![Mazes](https://gitlab.fit.cvut.cz/maherula/maze/raw/master/Mazes.png)