#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>

#define TFT_CS     10
#define TFT_RST    9  
#define TFT_DC     8
#define TFT_SCLK   13
#define TFT_MOSI   11

#define RX 0
#define TX 1

#define UP     5
#define DOWN   3
#define LEFT   4
#define RIGHT  2

#define CHEAT_PIN 7

#define ST7735_GREY tft.Color565(80, 80, 80)

#define N_MAZES 9

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

struct Point{
	uint8_t row;
	uint8_t col;
}finish_point, player_pos;

struct Maze{
	struct Point points[2];
	char walls_v[6];
	char walls_h[5];
}maze;

void set_maze(int num){
	switch (num) {
	    case 0:
			maze.points[0].row = 1;
			maze.points[0].col = 0;

			maze.points[1].row = 2;
			maze.points[1].col = 5;

			maze.walls_v[0] = 0x04; // 00100
			maze.walls_v[1] = 0x05; // 10100
			maze.walls_v[2] = 0x05; // 10100
			maze.walls_v[3] = 0x09; // 10010
			maze.walls_v[4] = 0x14; // 00101
			maze.walls_v[5] = 0x0a; // 01010

			maze.walls_h[0] = 0x32; // 010011
			maze.walls_h[1] = 0x1c; // 001110
			maze.walls_h[2] = 0x12; // 010010
			maze.walls_h[3] = 0x1e; // 011110
			maze.walls_h[4] = 0x12; // 010010
	      break;
	    case 1:
			maze.points[0].row = 1;
			maze.points[0].col = 4;

			maze.points[1].row = 3;
			maze.points[1].col = 1;		

			maze.walls_v[0] = 0x04; //00100
			maze.walls_v[1] = 0x0a; //01010
			maze.walls_v[2] = 0x05; //10100
			maze.walls_v[3] = 0x1a; //01011
			maze.walls_v[4] = 0x17; //11101
			maze.walls_v[5] = 0x05; //10100

			maze.walls_h[0] = 0x25; //101001
			maze.walls_h[1] = 0x1a; //010110
			maze.walls_h[2] = 0x14; //001010
			maze.walls_h[3] = 0x0a; //010100
			maze.walls_h[4] = 0x10; //000010
	      break;
	    case 2:
			maze.points[0].row = 3;
			maze.points[0].col = 3;

			maze.points[1].row = 3;
			maze.points[1].col = 5;		

			maze.walls_v[0] = 0x0c; //00110
			maze.walls_v[1] = 0x17; //11101
			maze.walls_v[2] = 0x16; //01101
			maze.walls_v[3] = 0x1f; //11111
			maze.walls_v[4] = 0x1d; //10111
			maze.walls_v[5] = 0x08; //00010

			maze.walls_h[0] = 0x02; //010000
			maze.walls_h[1] = 0x19; //100110
			maze.walls_h[2] = 0x00; //000000
			maze.walls_h[3] = 0x00; //000000
			maze.walls_h[4] = 0x06; //011000
	      break;

		case 3:
			maze.points[0].row = 0;
			maze.points[0].col = 0;

			maze.points[1].row = 3;
			maze.points[1].col = 0;  

			maze.walls_v[0] = 0x02; //01000
			maze.walls_v[1] = 0x03; //11000
			maze.walls_v[2] = 0x15; //10101
			maze.walls_v[3] = 0x01; //10000
			maze.walls_v[4] = 0x10; //00001
			maze.walls_v[5] = 0x14; //00101

			maze.walls_h[0] = 0x1c; //001110
			maze.walls_h[1] = 0x18; //000110
			maze.walls_h[2] = 0x16; //011010
			maze.walls_h[3] = 0x1e; //011110
			maze.walls_h[4] = 0x0e; //011100
		  break;

		case 4:
			maze.points[0].row = 2;
			maze.points[0].col = 4;

			maze.points[1].row = 5;
			maze.points[1].col = 3;  

			maze.walls_v[0] = 0x00; //00000
			maze.walls_v[1] = 0x10; //00001
			maze.walls_v[2] = 0x0a; //01010
			maze.walls_v[3] = 0x19; //10011
			maze.walls_v[4] = 0x11; //10001
			maze.walls_v[5] = 0x01; //10000

			maze.walls_h[0] = 0x0f; //111100
			maze.walls_h[1] = 0x36; //011011
			maze.walls_h[2] = 0x0c; //001100
			maze.walls_h[3] = 0x16; //011010
			maze.walls_h[4] = 0x1c; //001110
		  break;

    	case 5:
			maze.points[0].row = 0;
			maze.points[0].col = 4;

			maze.points[1].row = 4;
			maze.points[1].col = 2;  

			maze.walls_v[0] = 0x05; //10100
			maze.walls_v[1] = 0x17; //11101
			maze.walls_v[2] = 0x0e; //01110
			maze.walls_v[3] = 0x1a; //01011
			maze.walls_v[4] = 0x0e; //01110
			maze.walls_v[5] = 0x08; //00010

			maze.walls_h[0] = 0x08; //000100
			maze.walls_h[1] = 0x10; //000010
			maze.walls_h[2] = 0x26; //011001
			maze.walls_h[3] = 0x01; //100000
			maze.walls_h[4] = 0x16; //011010
          break;

        case 6:
			maze.points[0].row = 0;
			maze.points[0].col = 1;

			maze.points[1].row = 5;
			maze.points[1].col = 1;  

			maze.walls_v[0] = 0x08; //00010
			maze.walls_v[1] = 0x15; //10101
			maze.walls_v[2] = 0x0a; //01010
			maze.walls_v[3] = 0x12; //01001
			maze.walls_v[4] = 0x13; //11001
			maze.walls_v[5] = 0x00; //00000

			maze.walls_h[0] = 0x06; //011000
			maze.walls_h[1] = 0x1c; //001110
			maze.walls_h[2] = 0x2b; //110101
			maze.walls_h[3] = 0x18; //000110
			maze.walls_h[4] = 0x0e; //011100
          break;

        case 7:
			maze.points[0].row = 0;
			maze.points[0].col = 3;

			maze.points[1].row = 3;
			maze.points[1].col = 2;  

			maze.walls_v[0] = 0x09; //10010
			maze.walls_v[1] = 0x14; //00101
			maze.walls_v[2] = 0x11; //10001
			maze.walls_v[3] = 0x05; //10100
			maze.walls_v[4] = 0x03; //11000
			maze.walls_v[5] = 0x00; //00000

			maze.walls_h[0] = 0x04; //001000
			maze.walls_h[1] = 0x1e; //011110
			maze.walls_h[2] = 0x0c; //001100
			maze.walls_h[3] = 0x3a; //010111
			maze.walls_h[4] = 0x3c; //001111
          break;

        case 8:
			maze.points[0].row = 1;
			maze.points[0].col = 2;

			maze.points[1].row = 4;
			maze.points[1].col = 0;  

			maze.walls_v[0] = 0x01; //10000
			maze.walls_v[1] = 0x1b; //11011
			maze.walls_v[2] = 0x14; //00101
			maze.walls_v[3] = 0x0b; //11010
			maze.walls_v[4] = 0x17; //11101
			maze.walls_v[5] = 0x0a; //01010

			maze.walls_h[0] = 0x0c; //001100
			maze.walls_h[1] = 0x08; //000100
			maze.walls_h[2] = 0x16; //011010
			maze.walls_h[3] = 0x18; //000110
			maze.walls_h[4] = 0x20; //000001
          break;
	}
}

const uint8_t rec_size = 9; // rectangle size (odd number)
const uint8_t rec_border = (19-rec_size)/2;  // rectangle border size 
const uint8_t rec_wb = rec_size + rec_border*2; // rectangle with borders size
const uint8_t h_offset = ( tft.width() - (rec_size*6 + rec_border*2*5) ) / 2;
const uint8_t v_offset = ( tft.height() - (rec_size*6 + rec_border*2*5) ) / 2;

void draw_rect(int row, int col, uint16_t color){
	const uint8_t x = h_offset + col*rec_wb;
	const uint8_t y = v_offset + row*rec_wb;
	tft.fillRect(x, y, rec_size, rec_size, color);
}

void draw_circ(int row, int col, uint16_t color){
	const uint8_t x = h_offset + rec_size/2 + col*rec_wb;
	const uint8_t y = v_offset + rec_size/2 + row*rec_wb;
	const uint8_t r = ceil(sqrt(rec_size*rec_size*2)/2.0) ;
	tft.drawCircle(x, y, r+1, color);
	// tft.drawCircle(h_offset+5+col*rec_wb, v_offset+5+row*rec_wb, 9, color);
}

void draw_vertical_wall(int row, int col, uint16_t color){
	const uint8_t x = h_offset + rec_wb - (rec_border+1) + col*rec_wb;
	const uint8_t y = v_offset - (rec_border-1) + row*rec_wb;
	tft.fillRect(x, y, 2, rec_wb-2, color);
}

void draw_horizontal_wall(int row, int col, uint16_t color){
	const uint8_t x = h_offset - (rec_border-1) + col*rec_wb;
	const uint8_t y = v_offset + rec_wb - (rec_border+1) + row*rec_wb;
	tft.fillRect(x, y, rec_wb-2, 2, color);
}

void display_points(void){
	for(int i=0; i<6; ++i)
		for(int j=0; j<6; ++j)
			draw_rect(i, j, ST7735_GREY);
}

void display_marks(void){
	for(int i=0; i<2; ++i){
		struct Point p = maze.points[i];
		draw_circ(p.row, p.col, ST7735_GREEN);
	}
}

void display_finish(uint16_t color = ST7735_RED){
	draw_rect(finish_point.row, finish_point.col, color);
}

void display_walls(void){
	const int color = tft.Color565(100, 20, 20);
	for(int row=0; row<6; ++row){
		for(int col=0; col<6; ++col){
			if( col<5 ){
				if( maze.walls_v[row]&(1<<col) ){
					draw_vertical_wall(row, col, color);
				}
			}
			if( row<5 ){
				if( maze.walls_h[row]&(1<<col) ){
					draw_horizontal_wall(row, col, color);
				}
			}
		}
		
	}
}

void display_player(void){
	draw_rect(player_pos.row, player_pos.col, ST7735_YELLOW);
}

void erase_player(void){
	draw_rect(player_pos.row, player_pos.col, ST7735_GREY);
}

struct Point random_point(void){
	struct Point p;
	p.row = random(0, 5);
	p.col = random(0, 5);
	return p;
}

bool isFinish(void){
	if ( player_pos.row == finish_point.row &&
		 player_pos.col == finish_point.col )
		return true;
	return false;
}

void init_finish(void){
	finish_point = random_point();
}

void init_player(void){
	do {
		player_pos = random_point();	
	} while( isFinish() );
}

byte buttonFlag = 0;

bool buttonEvent(int button){
    switch (button)
    {
    case UP:
        if (digitalRead(UP) == LOW)
        {
            buttonFlag |= 1;
        }
        else if (buttonFlag & 1)
        {
            buttonFlag ^= 1;
            return true;
        }
        break;

    case DOWN:
        if (digitalRead(DOWN) == LOW)
        {
            buttonFlag |= 2;
        }
        else if (buttonFlag & 2)
        {
            buttonFlag ^= 2;
            return true;
        }
        break;

    case LEFT:
        if (digitalRead(LEFT) == LOW)
        {
            buttonFlag |= 4;
        }
        else if (buttonFlag & 4)
        {
            buttonFlag ^= 4;
            return true;
        }
        break;

    case RIGHT:
        if (digitalRead(RIGHT) == LOW)
        {
            buttonFlag |= 8;
        }
        else if (buttonFlag & 8)
        {
            buttonFlag ^= 8;
            return true;
        }
    }
    return false;
}


int cheats(void){
	return digitalRead(CHEAT_PIN) == LOW;
}

uint8_t mistakes = 0;

void init_game( uint8_t maze_number = random(0, N_MAZES) ){
	mistakes = 0;

	tft.fillScreen(ST7735_BLACK);

	if( cheats() ){
		tft.setCursor(4, 4);
		tft.setTextColor(ST7735_YELLOW);
		tft.setTextSize(1);
		tft.println(maze_number);
	}

	set_maze(maze_number);
	display_marks();
	display_points();
	init_finish();
	display_finish();
	init_player();
	display_player();
	if( cheats() )
		display_walls();

	
}

void move_player(uint8_t way){
	erase_player();
	switch (way) {
	    case UP:
	      --player_pos.row;
	      break;
	    case DOWN:
	      ++player_pos.row;
	      break;
	    case LEFT:
	      --player_pos.col;
	      break;
	    case RIGHT:
	      ++player_pos.col;
	      break;
	}
	display_player();
	delay(40);
}

void player_control(void){
	if ( buttonEvent(UP) ){
	if ( player_pos.row > 0 ){
		if ( maze.walls_h[player_pos.row-1] & 1<<player_pos.col ){
			++mistakes;
			draw_horizontal_wall(player_pos.row-1, player_pos.col, ST7735_RED);
		} else {
			move_player(UP);
		}
	}
	}else if ( buttonEvent(DOWN) ){
		if ( player_pos.row < 5 ){
			if ( maze.walls_h[player_pos.row] & 1<<player_pos.col ){
				++mistakes;
				draw_horizontal_wall(player_pos.row, player_pos.col, ST7735_RED);
			} else {
				move_player(DOWN);
			}
		}
	} else if ( buttonEvent(LEFT) ){
		if ( player_pos.col > 0 ){
			if ( maze.walls_v[player_pos.row] & 1<<(player_pos.col-1) ){
				++mistakes;
				draw_vertical_wall(player_pos.row, player_pos.col-1, ST7735_RED);
			} else {
				move_player(LEFT);
			}
		}
	} else if ( buttonEvent(RIGHT) ){
		if ( player_pos.col < 5 ){
			if ( maze.walls_v[player_pos.row] & 1<<(player_pos.col) ){
				++mistakes;
				draw_vertical_wall(player_pos.row, player_pos.col, ST7735_RED);
			} else {
				move_player(RIGHT);
			}
		}
	}
}


uint8_t master_connected = 0;
uint8_t module_number = 0;
uint8_t init_recived = 0;

byte frame[20];

void setup() {
	pinMode(TX, OUTPUT);
	pinMode(RX, INPUT);
	digitalWrite(TX, HIGH);
	digitalWrite(RX, LOW);

	delay(3000);
	if ( digitalRead(RX) == HIGH ){
		master_connected = 1;
	}
	delay(1000);

	digitalWrite(TX, LOW);
	Serial.begin(9600);
	
	randomSeed(analogRead(0));
	tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab

	pinMode(UP, INPUT_PULLUP);
	pinMode(DOWN, INPUT_PULLUP);
	pinMode(LEFT, INPUT_PULLUP);
	pinMode(RIGHT, INPUT_PULLUP);

	pinMode(CHEAT_PIN, INPUT_PULLUP);

	init_game();
}

uint8_t recive_frame(){
	if( Serial.available() >= 3 ){
		Serial.readBytes(frame, 3);
		if(frame[2] > 0)
			Serial.readBytes(frame+3, frame[2]);
		return 3+frame[2];
	}
	return 0;
}

void send_frame(){
  for (int i = 0; i < 3 + frame[2]; i++) { // 3 + SIZE(PARAM)
    Serial.write(frame[i]);	
  }
}

uint8_t recive_resend_init(){
	if( recive_frame() ){
		if( frame[1] != 1 ) // Not init message
			return 0;
		module_number = frame[3];
		frame[3]++;
		send_frame();
		return 1;
	}
	return 0;
}

uint8_t recive_resend_message(){
	if( recive_frame() ){
		if ( frame[0] == 0 ){
			if ( frame[1] == 2 )   // Boom!!
				init_recived = 0;  // Wait for next init in loop()
				tft.fillRect(0, 4, 2, 3, ST7735_BLACK); // Erase init indicator
			send_frame();
			return 1;
		}else if ( frame[0] != module_number ){
			send_frame();
			return 0;
		}
	}
	return 0;

}

void send_strike(){
	frame[0] = 1; // Master module
	frame[1] = 4; // Strike
	frame[2] = 1; // Size = 1
	frame[4] = module_number; // Send this module adress
	send_frame();
}

void send_solved(){
	frame[0] = 1; // Master module
	frame[1] = 6; // Solved
	frame[2] = 1; // Size = 1
	frame[4] = module_number; // Send this module adress
	send_frame();
}

void loop() {
	if ( master_connected ){

	   	if(!init_recived){
	   		tft.fillRect(0, 0, 2, 3, ST7735_RED); // Indicate init waiting
	   		if( recive_resend_init() ){
	   			init_game();
		      	init_recived = true;
		      	tft.fillRect(0, 4, 2, 3, ST7735_GREEN); // Indicate init message recive
	   		}
	   		delay(100);
	   		tft.fillRect(0, 0, 2, 3, ST7735_YELLOW); // Indicate init waiting
	   		delay(100);
	    }else{

			recive_resend_message();

			if ( ! isFinish() ){   // While not finish
				uint8_t temp_mistakes = mistakes;
				player_control();  // Make step
				if ( temp_mistakes < mistakes )
					send_strike(); // Player mistake
				if ( isFinish() ){
					send_solved(); // Player finish in this step
					display_finish(ST7735_GREEN);
					init_recived = 0;
					tft.fillRect(0, 4, 2, 3, ST7735_BLACK); // Erase init indicator
				}
			}
	    }
	}
	else{
		if ( isFinish() ){
			display_finish(ST7735_GREEN);
			delay(400);
			init_game();
		}
		player_control();
	}
	
}